# sTGC Pad Charge Sharing
This page details the most recent implementation of the sTGC Pad charge sharing model found in the digitization package of the athena framework, developped by Alexandre Laurier and integrated into athena around March 2023. The sTGC digitization package in athena can be found [here](https://gitlab.cern.ch/atlas/athena/-/tree/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization). The implementation of the pad charge sharing affects the creation of pad digits in the [sTGCDigitMaker](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx) and is discussed in detail here.

The first official discussion and presentation of the work can be found in the sTGC Data Quality weekly meeting [here](https://indico.cern.ch/event/1257352/contributions/5280866/attachments/2601160/4491359/PadChargeSharing.pdf). These slides are also found in this git repository.

## Goal of the Implementation of sTGC Pad Charge Sharing
The goal of the sTGC digitization is to create a high fidelity description of the sTGC detector in athena. This must include the description of the physics of the charge distribution on the different readout channels. The charge distribution on the pads is not a described by a pointwise distribution, but rather some 2D spread. A decent approximation can be made that the distribution follows a 2D Gaussian distribution, directly leading to the conclusion that the charge can be shared between several readout channels.

To create a high fidelity description of the physics of charge sharing, we must first obtain and understand real data from the sTGC detector and then create a simplified model that we can apply to the sTGC digitization. The data available to us was obtained at test beams sessions at CERN and Fermilab, and summarized in the following [published paper](https://arxiv.org/pdf/1509.06329.pdf). Section 6.2 of this paper discusses pad charge sharing, with the results shown in the Figure 16 of that paper, and copied below.

![PaperSharing](/figures/FigureFromPaper.png){width=40%}

The end goal of this project is to replicate the physics described in this figure within the sTGC digitization.

## Physics / Description of charge sharing
### Replicating the Data figure
It was found by trial and error that the distribution from data is described by an error function. With a little bit of tuning, we can obtain a very good agreement between the plot from the paper and the output of the error function. The following figure shows a (visually unappealing) comparison between an error function and the plot from data. The figures are rescaled so the axes match, and thus we observe the very good agreement. In the figure, the blue line corresponds to a slightly modified error function.

![dataComparison](/figures/Comparison.png){width=40%}

*Comparison between error function and data function from the paper. The blue line is obtained with an error function.*

### Understanding the Data figure
Although a parameterization of the data is technically sufficient to start create a charge sharing model in the digitization, understanding the data will help us create a faithful model. We can explain the data by posing a simple assumption: the charge distribution on the pads electrode follows a 2D Gaussian distribution. The data curve is replicated by the cumulative charge fraction for $` x \geq x' `$. To give some meaning to this statement, it is equivalent to asking what is the fraction of charge on the right of the line defined by $`x=x'`$, where $`x=y=0`$ is the centre of the 2D Gaussian distribution, as shown in the figure below.

![](/figures/Qdistribution.png){width=40%}

*Model of the 2D Gaussian Charge Distribution. The cumulative charge fraction corresponds to the total charge fraction to the right side of the line at $`x=x'`$*

By calculating the cumulative charge fraction for every x and plotting the result, we get the same result as from the data. Hence, the cumulative charge fraction within a 2D gaussian can be described by the error function. **This fact was derived by Alain during his sabbatical, a nice confirmation of this work.** The figure below shows the approximate data curve, modelled with an error function, and compares it to the cumulative charge fraction for $` x \geq x' `$ in a 2D Gaussian distribution centered at $`x=y=0`$.

![](/figures/CCF.png){width=40%}

*Cumulative charge distribution of a 2D gaussian distribution. The approx. data curve corresponds to the parameterized data curve obtained with an error function. The charge fraction at any given x represents the fraction of charge for $` x' \geq x `$.*


The cumulative charge fraction of the 2D Gaussian distribution is found to be described by the following charge fraction function $`\mathrm{Q}_{\mathrm{frac}}(x)`$ describing the total charge fraction for $` x' \geq x `$ , where $` \sigma `$ corresponds to the width of the charge distribution. The parameterziation is given by

```math
\mathrm{Q}_{\mathrm{frac}}(x) = 0.5 * (1.0 - erf(\frac{x}{\sqrt{2}\sigma})).
```
Following this, we find that the distribution of data corresponds to a Gaussian width of approximately $` \sigma = 1.80~\mathrm{mm}`$ whereas the best current estimate of the charge width (on the strips) in the current sTGC detectors is instead $` \sigma = 2.27~\mathrm{mm}`$. This disagreement can be explained by several factors such as the simplified assumption of a fully symmetric 2D Gaussian distribution and differences in the grounding of pads and strips cathode boards.

### Interpretation of this result
The result of $`\mathrm{Q}_{\mathrm{frac}}(x)`$ returns the fraction of charge for  $` x' \geq x `$ in a fully symmetrical 2D Gaussian distribution centered at $`x=y=0`$. We use this function to calculate the fraction of charge on a pad that is described by having an edge following a vertical or horizontal line at  $` x = x' `$. This is the how the charge sharing is implemented within athena.

## Implementation of charge sharing in the sTGC Digitization
In this section, the links shared will point to code in a personnal git branch of athena. These changes are currently in a [merge request](https://gitlab.cern.ch/atlas/athena/-/merge_requests/61455) awaiting to be merged to athena. As such, I can not promise that the links will be valid in the future or accurately describe the current code in athena. The logic outlined below will however still be applicable.

When creating digits in the [sTgcDigitmaker](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx), the energy deposit from travelling particles, taking into account the wires, is is projected to the pad plane and called a hit. We can then evaluate the pads under the hit and make digits on these pads. When no pad charge sharing is included, each hit creates at most one pad digit, corresponding to the pad directly underneath the hit. The part the digitization code responsible for create pad digits can be found [here](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L526).

To instead create multiple digits per hit via pad charge sharing, we must evaluate whether or not a hit is near another pad by computing the distance to the pad segmentation lines. If the hit is indeed near a neighboring pad (or several), the charge fraction for the neighbor pad is computed using the charge fraction function described above. The following figure shows a sketch of a hit nearby a second pad such that a fraction of the total charge will be spread to the pad N+1 instead of being place only in pad N.

![](/figures/padSegmentation.png){width=40%}
*Pad segmentation for example in digi*

The pad [charge sharing implementation](getPadChargeFraction) in the digitization follows the following logic:
1. Given a hit, [find the nominal pad](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L540) under the hit that will hold most (or all) of the induced charge.
2. [Search for neighbor pads](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L565) that are within  $` 2.5 \sigma `$ of the hit position, where $` \sigma `$ is the [width of the charge distribution](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitMaker.h#L160).
3. If there are no neighboring pads in close proximity, [create a single digit](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitMaker.h#L160) containing all the charge of the cluster.
4. If there is only 1 neighboring pad in close proximity, [calculate the fraction](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitMaker.h#L160) of the total charge it should carry using the error function defined in the above section.
    - The getPadChargeFraction function returns the charge fraction for $` x' \geq x `$ where $` x `$ is the distance between the hit centre and the pad segmentation line. This can be applied both in the x and y direction.
    - The [fraction of charge on the main pad](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L612) (under the hit) is given by $` 1. - \mathrm{getPadChargeFraction}(x) `$.
    - The [fraction of charge on the neighbor pad](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L612) is given by $` \mathrm{getPadChargeFraction}(x) `$
5. If there are 3 neighboring pads in close proximity, corresponding to the hit being near the corner of the pad, calculate the calculate the charge fraction for both x and y directions. The charge fractions for each of the 4 pads are:
    - The fraction of charge on [the main pad](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L603) is given by $` 1. - \mathrm{getPadChargeFraction}(x) - \mathrm{getPadChargeFraction}(y) + \mathrm{getPadChargeFraction}(x) * \mathrm{getPadChargeFraction}(y)`$.
    - The fraction of charge on the [neighbor pad in x](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L600) given by $` \mathrm{getPadChargeFraction}(x) * (1. - \mathrm{getPadChargeFraction}(y))`$.
    - The fraction of charge on the [neighbor pad in y](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L601) given by $` \mathrm{getPadChargeFraction}(y) * (1. - \mathrm{getPadChargeFraction}(x))`$.
    - The fraction of charge on the [neighbor corner pad](https://gitlab.cern.ch/alaurier/athena/-/blob/sTGC_Pad_Sharing/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx#L602) is given by $`\mathrm{getPadChargeFraction}(x) * \mathrm{getPadChargeFraction}(y)`$.
6. Create one digit per pad that holds charge, with the appropriate charge fraction.
